#!/usr/bin/env ruby
# Amazon/AWS Report Parser, Summary Report Generator
# Version: 1.0
# Processes the aws-billing-detailed-line-items-with-resources-and-tags report csv
# (or zipped CSV) report file(s) and generates a summary report of bandwidth
# and storage usage broken up by user:client tag.
#
# Copyright (c) 2016, Bryce Chidester <bryce@cobryce.com>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.


# Basic arguments check -- no reason to load in gems yet.
abort("You must specify a file.\n#{$0} <file(s)>") if !ARGV[0]

require 'csv'
csv_options = { headers: :first_row,
                converters: :all,
                header_converters: :symbol,
                skip_blanks: true
              }

# Variables used for auditing/matching results against the original CSV
invoice_totals = 0.0
rounding_totals = 0.0

# Hashes of sums, keyed to each user:client tag
cost_per_client = Hash.new
bytes_per_client = Hash.new
bytesS_per_client = Hash.new
costS_per_client = Hash.new
bytesIA_per_client = Hash.new
costIA_per_client = Hash.new
bytesRRS_per_client = Hash.new
costRRS_per_client = Hash.new
bytesG_per_client = Hash.new
costG_per_client = Hash.new
transfer_per_client = Hash.new
transfer_in_per_client = Hash.new
cost_in_per_client = Hash.new
transfer_out_per_client = Hash.new
cost_out_per_client = Hash.new

ARGV.each { |arg|
	in_file = File.realpath(arg) if File.exists?(arg.to_s)
	abort("The specified file '#{arg}' could not be accessed. Aborting.") \
	    if !File.exists?(in_file.to_s)
	
	abort("You must provide the \"aws-billing-detailed-line-itmes-with-" \
	    "resources-and-tags\" CSV or ZIP file.") \
	    if !in_file[/aws\-billing\-detailed\-line\-items\-with\-resources\-and\-tags\-\d{4}\-\d{2}\.csv(\.zip)?$/]
	
	csv_data = nil
	if in_file[/\.zip$/]
		STDERR.puts "Reading ZIP file: #{in_file}"
		begin
			# Only need these gems if we're trying to deal with ZIP files
			require 'rubygems'
			require 'bundler/setup'
			require 'zip'
		rescue
			# bundler bombs out before this.
			abort("Unable to read ZIP files without the 'zip' gem.")
		end
		Zip::File.open(in_file) { |zip_file|
			csv_file = zip_file.glob("*-aws-billing-detailed-line-items-with-" \
			    "resources-and-tags-*.csv").first
			STDERR.puts "Reading contained CSV file: #{csv_file}"
			csv_data = CSV.new(csv_file.get_input_stream, csv_options)
		}
	elsif in_file[/\.csv$/]
		STDERR.puts "Reading CSV file: #{in_file}"
		#csv_data = File.read(in_file)
		#print CSV.table(in_file)
		csv_data = CSV.open(in_file, csv_options)
	else
		abort("Filename is expected to end in .zip or .csv. I'm not even sure " \
		    "how you made it this far in execution.")
	end
	
	# Iterate over each row of the CSV, summing/processing based on type
	csv_data.each { |r|
		
		# Store (sum) the "InvoiceTotal" so we can match the computed total
		# with Amazon's computed total
		if r[:recordtype] == "InvoiceTotal"
			invoice_totals += r[:cost].to_f
			next
		end
		
		# "This line contains rounding error due to the consolidated
		#  billing and hourly line item calculation processes."
		if r[:recordtype] == "Rounding"
			rounding_totals += r[:cost].to_f
			next
		end
		
		# Only process "LineItem," rows with usage data.
		next if r[:recordtype] != "LineItem"
		# Only process usage data with a user:client tag assigned
		next if r[:userclient] == ""
		
		# Use begin/rescue to catch hiccups and dump a bit more information
		begin
			cost_per_client[r[:userclient]] = \
			    cost_per_client[r[:userclient]].to_f + r[:cost].to_f
			bytes_per_client[r[:userclient]] = \
			    bytes_per_client[r[:userclient]].to_f + r[:usagequantity].to_f \
			    if r[:usagetype][/TimedStorage/]
			# Break out the storage usage by service type
			bytesS_per_client[r[:userclient]] = \
			    bytesS_per_client[r[:userclient]].to_f + r[:usagequantity].to_f \
			    if r[:usagetype][/TimedStorage/] && r[:operation][/StandardStorage/]
			costS_per_client[r[:userclient]] = \
			    costS_per_client[r[:userclient]].to_f + r[:cost].to_f \
			    if r[:usagetype][/TimedStorage/] && r[:operation][/StandardStorage/]
			bytesIA_per_client[r[:userclient]] = \
			    bytesIA_per_client[r[:userclient]].to_f + r[:usagequantity].to_f \
			    if r[:usagetype][/TimedStorage/] && r[:operation][/StandardIAStorage/]
			costIA_per_client[r[:userclient]] = \
			    costIA_per_client[r[:userclient]].to_f + r[:cost].to_f \
			    if r[:usagetype][/TimedStorage/] && r[:operation][/StandardIAStorage/]
			bytesRRS_per_client[r[:userclient]] = \
			    bytesRRS_per_client[r[:userclient]].to_f + r[:usagequantity].to_f \
			    if r[:usagetype][/TimedStorage/] && r[:operation][/ReducedRedundancyStorage/]
			costRRS_per_client[r[:userclient]] = \
			    costRRS_per_client[r[:userclient]].to_f + r[:cost].to_f \
			    if r[:usagetype][/TimedStorage/] && r[:operation][/ReducedRedundancyStorage/]
			bytesG_per_client[r[:userclient]] = \
			    bytesG_per_client[r[:userclient]].to_f + r[:usagequantity].to_f \
			    if r[:usagetype][/TimedStorage/] && r[:operation][/Glacier/]
			costG_per_client[r[:userclient]] = \
			    costG_per_client[r[:userclient]].to_f + r[:cost].to_f \
			    if r[:usagetype][/TimedStorage/] && r[:operation][/Glacier/]
			transfer_per_client[r[:userclient]] = \
			    transfer_per_client[r[:userclient]].to_f + r[:usagequantity].to_f \
			    if r[:usagetype][/DataTransfer/]
			# Break out the bandwidth by upload/download
			transfer_in_per_client[r[:userclient]] = \
			    transfer_in_per_client[r[:userclient]].to_f + r[:usagequantity].to_f \
			    if r[:usagetype][/DataTransfer\-In/]
			cost_in_per_client[r[:userclient]] = \
			    cost_in_per_client[r[:userclient]].to_f + r[:cost].to_f \
			    if r[:usagetype][/DataTransfer\-In/]
			transfer_out_per_client[r[:userclient]] = \
			    transfer_out_per_client[r[:userclient]].to_f + r[:usagequantity].to_f \
			    if r[:usagetype][/DataTransfer\-Out/]
			cost_out_per_client[r[:userclient]] = \
			    cost_out_per_client[r[:userclient]].to_f + r[:cost].to_f \
			    if r[:usagetype][/DataTransfer\-Out/]
		rescue Exception => e
			puts "Row: #{r.to_s()}"
			puts "Inspect View: #{r.inspect()}"
			raise e	# Escalate the Exception
		end
	}
	csv_data.close
}

# Sum our computed totals for comparison against Amazon's
report_total = 0.0
cost_per_client.each_value { |v| report_total += v }

# Create an alphabetically-sorted list of clients so we can iterate cleanly.
clients = cost_per_client.keys | bytes_per_client.keys | transfer_per_client.keys
clients.sort!

# This format breaks it out into multiple lines
=begin
clients.each { |k|
	puts "%s: Total Transfer = %8.3f GB  (%10.3f MB)" %
	    [k, transfer_per_client[k], transfer_per_client[k].to_f * 0.1]
	puts "%s: Total Storage = %8.3f GB  (%10.3f MB)" %
	    [k, bytes_per_client[k], bytes_per_client[k].to_f * 0.1]
	puts "%s: Total = $%2.2f" % [k, cost_per_client[k]]
}
=end

# This is the compact summary output.
=begin
clients.each { |k|
	puts "%10s Total: $%2.2f  Storage: %8.3fGB  Bandwidth: %8.3fGB" %
	    [k, cost_per_client[k], bytes_per_client[k].to_f, transfer_per_client[k].to_f]
}
=end

# This is a one-line summary of usage and cost broken-down by component
clients.each { |k|
	puts "%-10s Total: $%2.2f  Storage: %8.3fGB (Std: %8.3fGB $%2.4f  " \
	      "IA: %8.3fGB $%2.4f  RRS: %8.3fGB $%2.4f  G: %8.3fGB $%2.4f)  " \
	      "Bandwidth: %8.3fGB (Up: %8.3fGB $%2.4f  Down: %8.3fGB $%2.4f)" %
	    [k,
	     cost_per_client[k].to_f,
	     bytes_per_client[k].to_f,
	     bytesS_per_client[k].to_f, costS_per_client[k].to_f,
	     bytesIA_per_client[k].to_f, costIA_per_client[k].to_f,
	     bytesRRS_per_client[k].to_f, costRRS_per_client[k].to_f,
	     bytesG_per_client[k].to_f, costG_per_client[k].to_f,
	     transfer_per_client[k].to_f,
	     transfer_in_per_client[k].to_f, cost_in_per_client[k].to_f,
	     transfer_out_per_client[k].to_f, cost_out_per_client[k].to_f
	    ]
}

# Compare the computed totals with Amazon's totals
=begin
puts "Cost Report Generation Auditing: Matching our totals with the invoice's."
puts "Invoice totals:      #{invoice_totals}"
puts "Invoice rounding:    #{rounding_totals}"
puts "Adj. Invoice totals: #{invoice_totals - rounding_totals} (invoice totals - rounding)"
puts "Report total:        #{report_total}"
puts "Difference:          #{invoice_totals - rounding_totals - report_total}"
=end

# Dump the objects themselves, for debugging
=begin
puts ""
puts "cost_per_client:         #{cost_per_client}"
puts "bytes_per_client:        #{bytes_per_client}"
puts "bytesS_per_client:       #{bytesS_per_client}"
puts "costS_per_client:        #{costS_per_client}"
puts "bytesIA_per_client:      #{bytesIA_per_client}"
puts "costIA_per_client:       #{costIA_per_client}"
puts "bytesRRS_per_client:     #{bytesRRS_per_client}"
puts "costRRS_per_client:      #{costRRS_per_client}"
puts "bytesG_per_client:       #{bytesG_per_client}"
puts "costG_per_client:        #{costG_per_client}"
puts "transfer_per_client:     #{transfer_per_client}"
puts "transfer_in_per_client:  #{transfer_in_per_client}"
puts "cost_in_per_client:      #{cost_in_per_client}"
puts "transfer_out_per_client: #{transfer_out_per_client}"
puts "cost_out_per_client:     #{cost_out_per_client}"
puts ""
=end

